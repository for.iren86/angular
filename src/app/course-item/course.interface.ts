export default interface ICourse {
  id: string
  title: string
  creationDate: number
  duration: number
  description: string
  author: string
  rate: boolean
}
