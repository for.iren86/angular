import { Component, Input } from '@angular/core';
import ICourse from './course.interface';
import pipeDuration from '../helpers/pipeDuration';
import dateGenerator from '../helpers/dateGenerator';
import DataService from '../Servises/data.service';

@Component({
  selector: 'app-course-item',
  templateUrl: './course-item.component.html',
  styleUrls: ['./course-item.component.css'],
})

export default class CourseItemComponent {
  @Input() courseItem: ICourse = {} as ICourse;

  constructor(private dataService: DataService) {
  }

  isVideoCourse(course: ICourse) {
    if (course.title) {
      return course.title.includes('Video') || course.title.includes('video');
    }
    return false;
  }

  getCourseDuration(course: ICourse) {
    return pipeDuration(course.duration);
  }

  getCourseDate(course: ICourse) {
    return dateGenerator(course.creationDate);
  }

  deleteCourseItem(course: ICourse) {
    const isAgree: boolean = window.confirm('Do you really want to delete this course?');
    if (isAgree) {
      this.dataService.removeCourseItem(course);
    }
  }
}
