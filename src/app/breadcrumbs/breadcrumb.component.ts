import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { filter, distinctUntilChanged } from 'rxjs/operators';
import IBreadCrumb from './breadcrumb.interface';
import ICourse from '../course-item/course.interface';
import DataService from '../Servises/data.service';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.css'],
})
export default class BreadcrumbComponent implements OnInit {
  courses: Array<ICourse> = this.dataService.getCourseList();

  advertId: string = '';

  courseTitle: string = '';

  public breadcrumbs: IBreadCrumb[];

  constructor(
    private dataService: DataService,
    private router: Router,
    private route: ActivatedRoute,
  ) {
    this.breadcrumbs = this.buildBreadCrumb(this.route.root);
  }

  ngOnInit() {
    this.dataService.currentCourses.subscribe((coursesData: Array<ICourse>) => {
      this.courses = coursesData;
    });

    this.router.events.pipe(
      filter((event) => event instanceof NavigationEnd),
      distinctUntilChanged(),
    ).subscribe(() => {
      this.breadcrumbs = this.buildBreadCrumb(this.route.root);
    });
  }

  getCourse() {
    return this.dataService.getCourseItemById(this.advertId);
  }

  buildBreadCrumb(route: ActivatedRoute, url: string = '', breadcrumbs: IBreadCrumb[] = []): IBreadCrumb[] {
    let label = route.routeConfig && route.routeConfig.data ? route.routeConfig.data['breadcrumb'] : '';
    let path = route.routeConfig && route.routeConfig.data ? route.routeConfig.path : '';

    const lastRoutePart: any = path?.split('/').pop();
    const isDynamicRoute = lastRoutePart?.startsWith(':');
    if (isDynamicRoute && !!route.snapshot) {
      const paramName = lastRoutePart?.split(':')[1];
      if (paramName !== undefined) {
        path = path?.replace(lastRoutePart, route.snapshot.params[paramName]);
        if (paramName === 'id') {
          this.advertId = route.snapshot.params[paramName];
          const currentCourse = this.getCourse();
          label = currentCourse.title;
        } else {
          label = route.snapshot.params[paramName];
        }
      }
    }

    const nextUrl = path ? `${url}/${path}` : url;

    const breadcrumb: IBreadCrumb = {
      label,
      url: nextUrl,
    };
    const newBreadcrumbs = breadcrumb.label ? [...breadcrumbs, breadcrumb] : [...breadcrumbs];
    if (route.firstChild) {
      return this.buildBreadCrumb(route.firstChild, nextUrl, newBreadcrumbs);
    }
    return newBreadcrumbs;
  }
}
