export default interface IBreadCrumb {
  label: string;
  url: string;
}
