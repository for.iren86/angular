import { Component, OnInit } from '@angular/core';
import DataService from '../Servises/data.service';
import ICourse from '../course-item/course.interface';
import OrderByDate from '../order-by.pipe';
import FilterByName from '../filter.pipe';
import COURSES from './mock-courses';

@Component({
  selector: 'app-courses-list',
  templateUrl: './courses-list.component.html',
  styleUrls: ['./courses-list.component.css'],
})

export default class CoursesListComponent implements OnInit {
  courses: Array<ICourse> = this.dataService.getCourseList();

  numOfItemsShown: number = 3;

  loadMoreStep: number = 3;

  constructor(
    private dataService: DataService,
    private orderByDate: OrderByDate,
    private filterByName: FilterByName,
  ) {
  }

  ngOnInit() {
    this.dataService.currentCourses.subscribe((coursesData: Array<ICourse>) => {
      this.courses = this.orderByDate.transform(coursesData);
    });

    this.dataService.currentSearchQuery.subscribe((query) => {
      this.courses = this.filterByName.transform(this.orderByDate.transform(COURSES), query);
    });

    this.getData();
  }

  getData() {
    this.dataService.currentCourses.subscribe((coursesData: Array<ICourse>) => {
      this.courses = coursesData.slice(0, this.numOfItemsShown);
    });
  }

  loadMore() {
    this.numOfItemsShown += this.loadMoreStep;
    this.getData();
  }

  trackByFn(index: number, item: ICourse) {
    return item.id;
  }
}
