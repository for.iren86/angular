import Course from '../course-item/course.interface';

const COURSES: Course[] = [
  {
    id: 'test1',
    title: 'Video course test data title', // upper case first letter
    creationDate: 1657836000000, // '2022/07/15' YYYY/M/D
    duration: 30,
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore '
      + 'et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip '
      + 'ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat '
      + 'nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id '
      + 'est laborum.',
    author: 'Iryna',
    rate: false,
  },
  {
    id: 'test2',
    title: 'Ordinary1 course test data title',
    creationDate: 1655244000000, // '2022/06/15' YYYY/M/D
    duration: 60,
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore '
      + 'et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip '
      + 'ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat '
      + 'nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id '
      + 'est laborum.',
    author: 'Iryna',
    rate: true,
  },
  {
    id: 'test3',
    title: 'video course test data title', // lower case first letter
    creationDate: 1660514400000, // '2022/08/15' YYYY/M/D
    duration: 90,
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore '
      + 'et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip '
      + 'ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat '
      + 'nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id '
      + 'est laborum.',
    author: 'Iryna',
    rate: false,
  },
  {
    id: 'test4',
    title: 'Ordinary2 course test data title',
    creationDate: 1659304800000,
    duration: 5,
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore '
      + 'et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip '
      + 'ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat '
      + 'nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id '
      + 'est laborum.',
    author: 'Iryna',
    rate: false,
  },
  {
    id: 'test5',
    title: 'Ordinary3 course test data title',
    creationDate: 1665784800000, // '2022/10/15' YYYY/M/D
    duration: 101,
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore '
      + 'et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip '
      + 'ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat '
      + 'nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id '
      + 'est laborum.',
    author: 'Iryna',
    rate: true,
  },
  {
    id: 'test6',
    title: 'video course test data title',
    creationDate: 1659304800000,
    duration: 102,
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore '
      + 'et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip '
      + 'ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat '
      + 'nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id '
      + 'est laborum.',
    author: 'Iryna',
    rate: false,
  },
  {
    id: 'test7',
    title: 'Video course test data title',
    creationDate: 1659304800000,
    duration: 103,
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore '
      + 'et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip '
      + 'ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat '
      + 'nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id '
      + 'est laborum.',
    author: 'Iryna',
    rate: false,
  },
];

export default COURSES;
