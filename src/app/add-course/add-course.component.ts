import { Component, OnInit } from '@angular/core';
import { UUID } from 'angular2-uuid';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { formatDate } from '@angular/common';

import pipeDuration from '../helpers/pipeDuration';
import ICourse from '../course-item/course.interface';
import DataService from '../Servises/data.service';

@Component({
  selector: 'app-add-course',
  templateUrl: './add-course.component.html',
  styleUrls: ['./add-course.component.css'],
})
export default class AddCourseComponent implements OnInit {
  course: ICourse = {
    id: '',
    title: '',
    creationDate: 0,
    duration: 1,
    description: '',
    author: '',
    rate: false,
  };

  courses: Array<ICourse> = this.dataService.getCourseList();

  courseDuration: number = 1;

  defaultAuthor: string = 'Iryna V.';

  createCourseForm: any = FormGroup;

  title: any = FormControl;

  description: any = FormControl;

  date: any = FormControl;

  duration: any = FormControl;

  author: any = FormControl;

  isSubmitted: boolean = false;

  advertId: string = '';

  isAdvertExist = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private dataService: DataService,
  ) {
    this.advertId = this.route.snapshot.params['id'];
  }

  ngOnInit() {
    if (this.advertId) {
      this.isAdvertExist = true;
      this.course = this.dataService.getCourseItemById(this.advertId);
      this.fullFillFormControls();
    } else if (this.route.snapshot.params['new'] === undefined) {
      this.createFormControls();
    }
    this.createForm();
  }

  getCurrentDate() {
    const myDate = Date();
    return formatDate(myDate, 'yyyy-MM-dd', 'en-US');
  }

  getCourseDate(data: number) {
    const myDate = new Date(data);
    return formatDate(myDate, 'yyyy-MM-dd', 'en-US');
  }

  handleDurationEventValue(value: string) {
    this.courseDuration = parseInt(value, 10);
  }

  getCourseDuration() {
    return pipeDuration(this.courseDuration);
  }

  generateCourseId() {
    return UUID.UUID();
  }

  currentDateToMs(d: string) {
    if (d === '') {
      const date = new Date(d);
      return date.getTime();
    }
    const date = new Date(d);
    return date.getTime();
  }

  goHome(): void {
    this.router.navigate(['../'], { relativeTo: this.route });
  }

  createFormControls() {
    this.title = new FormControl('', [
      Validators.required,
      Validators.minLength(2),
    ]);
    this.description = new FormControl('', [
      Validators.required,
      Validators.minLength(2),
    ]);
    this.date = new FormControl(this.getCurrentDate());
    this.duration = new FormControl(this.courseDuration);
    this.author = new FormControl({ value: this.defaultAuthor, disabled: true });
  }

  fullFillFormControls() {
    this.title = new FormControl(this.course.title, [
      Validators.required,
      Validators.minLength(2),
    ]);
    this.description = new FormControl(this.course.description, [
      Validators.required,
      Validators.minLength(2),
    ]);
    this.date = new FormControl(this.getCourseDate(this.course.creationDate));
    this.duration = new FormControl(this.course.duration);
    this.author = new FormControl({ value: this.course.author, disabled: true });
  }

  createForm() {
    this.createCourseForm = new FormGroup({
      title: this.title,
      description: this.description,
      date: this.date,
      duration: this.duration,
      author: this.author,
    });
  }

  onSubmit() {
    if (this.createCourseForm.valid) {
      this.course.title = this.createCourseForm.value.title;
      this.course.description = this.createCourseForm.value.description;
      this.course.creationDate = this.currentDateToMs(this.createCourseForm.value.date);
      this.course.duration = this.createCourseForm.value.duration;
      this.course.author = this.createCourseForm.value.author;
      if (this.isAdvertExist) {
        this.dataService.updateCourseItem(this.course);
      } else {
        this.course.id = this.generateCourseId();
        this.course.rate = false;
        this.courses.push(this.course);
      }
      this.goHome();
    }
  }
}
