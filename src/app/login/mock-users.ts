import User from './user.interface';

const USERS: User[] = [
  {
    id: 100,
    firstName: 'Iryna',
    lastName: 'Volkodav',
    email: 'i.volkodav@gmail.com',
    password: 'test1',
  },
  {
    id: 200,
    firstName: 'Anatolii',
    lastName: 'Volkodav',
    email: 'v.volkodav@gmail.com',
    password: 'test2',
  },
  {
    id: 300,
    firstName: 'Olga',
    lastName: 'Noname',
    email: 'Noname@gmail.com',
    password: 'test3',
  },
];

export default USERS;
