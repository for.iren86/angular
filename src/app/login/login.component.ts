import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import AuthService from '../Servises/auth.service';
import IUser from './user.interface';
import TokenStorageService from '../Servises/token-storage.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export default class LoginComponent implements OnInit {
  user: IUser = {} as IUser;

  currentUser: { email: string; password: string; } = {
    email: '',
    password: '',
  };

  isAuthenticated: boolean = false;

  constructor(
    private router: Router,
    private authService: AuthService,
    private route: ActivatedRoute,
    private tokenStorage: TokenStorageService,
  ) {
  }

  ngOnInit() {
    this.authService.isAuthenticated.subscribe((authStatus: boolean) => {
      this.isAuthenticated = authStatus;
    });

    this.authService.currentUser.subscribe((user: any) => {
      this.user = user;
    });
  }

  handleUsernameEventValue(value: string) {
    this.currentUser.email = value;
  }

  handlePasswordEventValue(value: string) {
    this.currentUser.password = value;
  }

  goHome(): void {
    this.router.navigate(['/courses'], { relativeTo: this.route });
  }

  login() {
    if (this.getUser(this.currentUser.email, this.currentUser.password)) {
      this.authService.login();
      if (this.isAuthenticated) {
        this.authService.changeUser(this.user);
        this.tokenStorage.saveUser(this.currentUser);
        this.goHome();
      }
    }
  }

  getUser(email: string, password: string): IUser {
    this.user = this.authService.getUserInfo(email, password);
    return this.user;
  }
}
