import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  styleUrls: ['./app.component.css'],
  template: `
    <app-header></app-header>
    <app-breadcrumb></app-breadcrumb>
    <router-outlet></router-outlet>
    <router-outlet name="section"></router-outlet>
    <router-outlet name="main"></router-outlet>
    <app-footer></app-footer>
  `,
})
export default class AppComponent {
  title = 'angular';
}
