import { Component, OnInit } from '@angular/core';
import DataService from '../Servises/data.service';

@Component({
  selector: 'app-section',
  templateUrl: './section.component.html',
  styleUrls: ['./section.component.css'],
})
export default class SectionComponent implements OnInit {
  searchQuery: string = '';

  constructor(
    private data: DataService,
    private dataService: DataService,
  ) {
  }

  ngOnInit() {
  }

  searchCourse() {
    this.dataService.changeSearchQuery(this.searchQuery);
  }
}
