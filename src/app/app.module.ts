import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { LoggerModule, NgxLoggerLevel } from 'ngx-logger';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import AppRoutingModule from './app-routing.module';
import AppComponent from './app.component';
import HeaderComponent from './header/header.component';
import SectionComponent from './section/section.component';
import CoursesListComponent from './courses-list/courses-list.component';
import FooterComponent from './footer/footer.component';
import OrderByDate from './order-by.pipe';
import FilterByName from './filter.pipe';
import CourseItemComponent from './course-item/course-item.component';
import LoginComponent from './login/login.component';
import AddCourseComponent from './add-course/add-course.component';
import PagenotfoundComponent from './pagenotfound/pagenotfound.component';
import BreadcrumbsComponent from './breadcrumbs/breadcrumb.component';
import { LogoComponent } from './logo/logo.component';
import CourseBodyStyleDirective from './course-body-style.directive';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SectionComponent,
    CoursesListComponent,
    FooterComponent,
    OrderByDate,
    FilterByName,
    CourseItemComponent,
    LoginComponent,
    AddCourseComponent,
    PagenotfoundComponent,
    BreadcrumbsComponent,
    OrderByDate,
    LogoComponent,
    CourseBodyStyleDirective,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    FormsModule,
    LoggerModule.forRoot({
      serverLoggingUrl: '/api/logs',
      level: NgxLoggerLevel.DEBUG,
      serverLogLevel: NgxLoggerLevel.ERROR,
    }),
  ],
  bootstrap: [AppComponent],
  providers: [OrderByDate, FilterByName,
  ],
})
export default class AppModule {
}
