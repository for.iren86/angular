import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'orderByDate' })
export default class OrderByDate implements PipeTransform {
  transform(listOfItems: Array<any>): Array<any> {
    return listOfItems.sort(
      (a, b) => a.creationDate - b.creationDate,
    );
  }
}
