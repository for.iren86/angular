import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import COURSES from '../courses-list/mock-courses';
import ICourse from '../course-item/course.interface';

@Injectable({
  providedIn: 'root',
})
export default class DataService {
  courses = COURSES;

  private searchQuerySource = new BehaviorSubject('');

  currentSearchQuery = this.searchQuerySource.asObservable();

  private coursesSource = new BehaviorSubject(this.courses);

  currentCourses = this.coursesSource.asObservable();

  constructor() {
  }

  changeSearchQuery(query: string) {
    this.searchQuerySource.next(query);
  }

  getCourseList() {
    return this.courses;
  }

  createCourse(item: ICourse): void {
    this.courses.push(item);
  }

  getCourseItemById(itemId: string | null) {
    const idx: number = this.courses.findIndex((x) => x.id === itemId);
    return this.courses[idx];
  }

  removeCourseItem(item: ICourse): void {
    const index = this.courses.indexOf(item, 0);
    if (index > -1) {
      this.courses.splice(index, 1);
      this.coursesSource.next(this.courses);
    }
  }

  updateCourseItem(item: ICourse): void {
    const itemIndex: number = this.courses.findIndex((x) => x.id === item.id);
    this.courses[itemIndex] = item;
  }
}
