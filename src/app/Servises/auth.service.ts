import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import USERS from '../login/mock-users';
import IUser from '../login/user.interface';
import TokenStorageService from './token-storage.service';

@Injectable({
  providedIn: 'root',
})
export default class AuthService {
  endpoint: string = 'http://localhost:4200/api';

  headers = new HttpHeaders().set('Content-Type', 'application/json');

  users = USERS;

  private authSource = new BehaviorSubject(false);

  isAuthenticated = this.authSource.asObservable();

  private userSource = new BehaviorSubject({});

  currentUser = this.userSource.asObservable();

  constructor(
    private http: HttpClient,
    public router: Router,
    private tokenStorage: TokenStorageService,
  ) {
  }

  login(): void {
    this.authSource.next(true);
  }

  doLogout() {
    this.tokenStorage.removeToken();
    if (this.tokenStorage.getToken() === null) {
      this.authSource.next(false);
      this.router.navigate(['/login']);
    }
  }

  changeUser(user: IUser) {
    this.userSource.next(user);
  }

  getUserInfo(userEmail: string, userPassword: string): IUser {
    const itemIndex: number = this.users.findIndex(
      (item) => item.email === userEmail && item.password === userPassword,
    );
    return this.users[itemIndex];
  }
}
