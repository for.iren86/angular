import { Component, OnInit } from '@angular/core';
import AuthService from '../Servises/auth.service';
import IUser from '../login/user.interface';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export default class HeaderComponent implements OnInit {
  isAuthenticated = false;

  user: IUser = {} as IUser;

  constructor(
    private authService: AuthService,
  ) {
  }

  ngOnInit(): void {
    this.authService.isAuthenticated.subscribe((status: boolean) => {
      this.isAuthenticated = status;
    });

    this.authService.currentUser.subscribe((user: any) => {
      this.user = user;
    });
  }

  logout() {
    this.authService.doLogout();
  }
}
