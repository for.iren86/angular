import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'filterByName' })
export default class FilterByName implements PipeTransform {
  transform(listOfItems: Array<any>, name: string): Array<any> {
    return listOfItems.filter(
      (courseItem) => courseItem.title.toLowerCase().includes(name.toLowerCase()),
    );
  }
}
