const formatDate = (dateInMs: number): string => {
  const myDate = new Date(dateInMs);
  const month: Array<string> = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September',
    'October', 'November', 'December'];
  return `${(`${myDate.getDate()}`).slice(-2)} ${(`${month[myDate.getMonth()]}`)}, ${myDate.getFullYear()}`;
};

export default formatDate;
