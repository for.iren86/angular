const pipeDuration = (min: number) => {
  if (min > 0) {
    const h: number = Math.floor(min / 60);
    const m: number = min % 60;
    const mDuration: any = m < 10 ? `0${m}` : m;
    if (h < 1) {
      return `${mDuration} min`;
    }
    return `${h} h ${mDuration} min`;
  }
  return '00 min';
};

export default pipeDuration;
