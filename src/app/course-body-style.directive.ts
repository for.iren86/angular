import {
  Directive, ElementRef, Renderer2, Input,
} from '@angular/core';
import ICourse from './course-item/course.interface';

@Directive({
  selector: '[appBorderStyle]',
})

export default class CourseBodyStyleDirective {
  @Input() course!: ICourse;

  currentDate: any = Date.now();

  constructor(private elRef: ElementRef, private renderer: Renderer2) {
  }

  ngOnInit() {
    this.setCourseStyle();
  }

  setCourseStyle() {
    if (this.isCourseNew()) {
      this.renderer.setStyle(this.elRef.nativeElement, 'border-width', '0.2rem');
      this.renderer.setStyle(this.elRef.nativeElement, 'border-style', 'solid');
      this.renderer.setStyle(this.elRef.nativeElement, 'border-color', 'green');
    } else if (this.isCourseUpcoming()) {
      this.renderer.setStyle(this.elRef.nativeElement, 'border-width', '0.2rem');
      this.renderer.setStyle(this.elRef.nativeElement, 'border-style', 'solid');
      this.renderer.setStyle(this.elRef.nativeElement, 'border-color', 'blue');
    } else if (this.isCourseRated()) {
      this.renderer.setStyle(this.elRef.nativeElement, 'background-color', 'lightyellow');
    }
  }

  daysToMilliseconds(days: number): number {
    return days * 24 * 60 * 60 * 1000;
  }

  isCourseNew() {
    return this.course.creationDate < this.currentDate
      && this.course.creationDate >= (this.currentDate - this.daysToMilliseconds(14));
  }

  isCourseUpcoming() {
    return this.course.creationDate > this.currentDate;
  }

  isCourseRated() {
    return this.course.rate;
  }
}
