import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import LoginComponent from './login/login.component';
import SectionComponent from './section/section.component';
import CoursesListComponent from './courses-list/courses-list.component';
import AddCourseComponent from './add-course/add-course.component';
import PagenotfoundComponent from './pagenotfound/pagenotfound.component';

const routes: Routes = [
  {
    path: 'courses',
    data: { breadcrumb: 'Courses' },
    children: [
      { path: '', component: SectionComponent, outlet: 'section' },
      { path: '', component: CoursesListComponent, outlet: 'main' },
    ],
  },
  {
    path: 'courses',
    data: { breadcrumb: 'Courses' },
    children: [
      {
        path: 'new',
        component: AddCourseComponent,
        data: {
          breadcrumb: '',
        },
      },
      {
        path: ':id',
        component: AddCourseComponent,
        data: {
          breadcrumb: '',
        },
      },
    ],
  },
  { path: 'login', component: LoginComponent },
  {
    path: '',
    redirectTo: '/courses',
    pathMatch: 'full',
  },
  {
    path: '**',
    pathMatch: 'full',
    component: PagenotfoundComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export default class AppRoutingModule {
}
